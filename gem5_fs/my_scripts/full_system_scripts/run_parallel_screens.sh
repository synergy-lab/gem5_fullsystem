#!/bin/bash

################################################################
## Script to launch a screen and run a command on it automatically
## Author: Tushar Krishna
## Last Updated: March 12, 2016
################################################################

if [ $# -ne 3 ]
then
    echo Usage: ./run_parallel_screens script \"argument1 argument2 ..\" session_name
    echo Example: ./run_parallel_screens ~/gem5_fs_resources/scripts/checkpoint_scripts/parsec2.1/create_checkpoints.sh \"blackscholes\" sim
    exit
fi

script=$1
benchmark=$2
name=$3

# Command
screen -dmS "run_${name}"
screen -S "run_$name" -p 0 -X stuff "$script $benchmark
"
