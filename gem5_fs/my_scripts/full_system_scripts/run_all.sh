#!/bin/bash

################################################################
## Script to run launch parsec benchmarks on different screens
## Author: Tushar Krishna
## Last Updated: Jul 21, 2016
#################################################################

## Output stats directory.
# If you expect to generate lot of stats, use the scratch directory
# Else use some folder in your home directory.
outdir='/usr/scratch/tushar/STATS/example' # CHANGEME

# Scripts to launch
script='./my_scripts/full_system_scripts/run_parallel_screens.sh ./my_scripts/full_system_scripts/run_parsec.sh'

# Number of cores
num_cores=16

# benchmark work_items
for benchmark in 'blackscholes 1000' 'bodytrack 300' 'canneal 1000' 'facesim 100' 'fluidanimate 1000' 'streamcluster 300' 'swaptions 500'
do
    bench_name=${benchmark/ /_} ## Converts the space to an underscore
    screen_name=${bench_name}_${num_cores}c # Unique name to each screen

    # print the command to run.
    # Note: You need to explicitly copy the generated command and paste it 
    # on the command line to run it.
    echo $script \"$num_cores $benchmark $outdir\" ${screen_name}
done
