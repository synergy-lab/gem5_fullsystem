#!/bin/bash

################################################################
## Script to run parsec benchmarks from checkpoints
## Author: Tushar Krishna
## Last Updated: July 21, 2016
#################################################################

if [ $# -ne 4 ] 
then 
    echo Usage: ./run_parsec.sh num_cores benchmark num_work_items out_dir
    exit
fi

# Input parameters
num_cores=$1
benchmark=$2 #options: 'blackscholes' 'bodytrack' 'canneal' 'dedup' 'facesim' 'ferret' 'fluidanimate' 'freqmine' 'streamcluster' 'swaptions' 'vips' 'x264' 'rtview'
work_items=$3
out_dir=$4

# Other Parameters
inp='simsmall' #options: 'simdev' 'simsmall' 'simmedium' 'simlarge
checkpoint_dir=/nethome/tkrishna3/gem5_fs_resources/checkpoints/x86-linux/${num_cores}c-1GB/parsec_roi/$inp/x86-linux_${num_cores}c-1GB_$benchmark
gem5_bin='X86_MOESI_CMP_directory/gem5.fast'

# Cache Parameters
# L1
l1_size='32kB'
l1_assoc=4

# Total L2 = 8MB
# L2 per tile:
if [ ${num_cores} -eq '64' ]; then
    l2_size='128kB'
elif [ ${num_cores} -eq '32' ]; then
    l2_size='256kB'
elif [ ${num_cores} -eq '16' ]; then
    l2_size='512kB'
else
    l2_size='128kB'
fi

l2_assoc=8


echo "Running gem5 for ${benchmark}_${inp}..."

## Run Command
./build/$gem5_bin -d $out_dir/${benchmark}_${num_cores}c_${inp}_w${work_items} configs/example/fs.py --num-cpus=${num_cores} --num-dirs=4 --mem-size=1GB --checkpoint-dir=$checkpoint_dir --checkpoint-restore=1 --work-end-exit-count=$work_items --ruby --restore-with-cpu timing --topology=MeshDirCorners --mesh-rows=8 --num-l2caches=$num_cores --l1d_size=${l1_size} --l1i_size=${l1_size} --l1d_assoc=${l1_assoc} --l1i_assoc=${l1_assoc} --l2_size=${l2_size} --l2_assoc=${l2_assoc}
