#!/bin/bash

################################################################
# Script to generate checkpoints for benchmarks
# Author: Tushar Krishna
#################################################################

#if [ $# -ne 1 ] 
#then 
#    echo Usage: ./create_checkpoint.sh benchmark
#    exit
#fi

#benchmark=$1

gem5_fs_resources_dir=/nethome/tkrishna3/gem5_fs_resources
cores=32
mem=1GB
config=${cores}c-${mem}

for inp in 'simsmall' 'simdev' 'simmedium' 'simlarge'
do
    for benchmark in 'blackscholes' 'bodytrack' 'canneal' 'dedup' 'facesim' 'ferret' 'fluidanimate' 'freqmine' 'streamcluster' 'swaptions' 'vips' 'x264' 'rtview'
    do
        echo "Running gem5 for $benchmark_$inp..."
        ./build/X86/gem5.fast -d $gem5_fs_resources_dir/checkpoints/x86-linux/$config/parsec_roi/$inp/x86-linux_${config}_$benchmark configs/example/fs.py --num-cpus=$cores --num-dirs=4 --mem-size=$mem --work-begin-exit-count=1 --checkpoint-at-end --script=$gem5_fs_resources_dir/scripts/checkpoint_scripts/parsec2.1/rcs_scripts/${benchmark}_${cores}c_${inp}_ckpts.rcS
        echo "Completed ${benchmark}_${inp}..." >> TEMP/ckpt_status.txt
    done
done
