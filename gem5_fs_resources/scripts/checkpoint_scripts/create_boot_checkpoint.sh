#!/bin/bash

################################################################
# Script to generate checkpoints for benchmarks
# Author: Tushar Krishna
#################################################################

./build/X86/gem5.fast -d /nethome/tkrishna3/gem5_fs_resources/checkpoints/x86-linux/64c-1GB/boot/x86-linux_post-boot_64c_1GB configs/example/fs.py --num-cpus=64 --num-dirs=4 --mem-size=1GB --checkpoint-at-end 
