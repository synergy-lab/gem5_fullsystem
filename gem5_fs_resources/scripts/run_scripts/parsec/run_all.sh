#!/bin/bash

################################################################
## Script to run launch parsec benchmarks on different screens
## Author: Tushar Krishna
## Last Updated: Jul 21, 2016
#################################################################

outdir='/usr/scratch/<username>/stats_dir'
script='./my_scripts/full_system_scripts/run_parallel_screens.sh ./my_scripts/full_system_scripts/run_parsec.sh'
num_cores=16

for benchmark in 'blackscholes 1000' 'bodytrack 300' 'canneal 1000' 'facesim 100' 'fluidanimate 1000' 'streamcluster 300' 'swaptions 500'
do
    bench_name=${benchmark/ /_} ## Converts the space to an underscore
    screen_name=${bench_name}
    echo $script \"$num_cores $benchmark $outdir\" ${screen_name}
done
