#### Example Scripts ####

## basic script to run a parsec benchmark for a specified number of work items.
## use this as a template to write your own script.
run_parsec.sh

## Note: use the following 2 helper scripts only at the point when you are ready to launch multiple simulations in parallel and you know 
there are no errors in the scripts or configurations.
# script to launch a new screen and run a script on it.
run_parallel_screens.sh

# runs all parsec benchmarks, one on each screen.
run_all.sh
