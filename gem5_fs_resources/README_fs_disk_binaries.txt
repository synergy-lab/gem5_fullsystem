Full-System Binaries and Disks for PARSEC2.1 for x86 linux

Step 1: 
wget http://www.m5sim.org/dist/current/m5_system_2.0b3.tar.bz2

Step 2:
tar jxvf m5_system_2.0b3.tar.bz2
    * creates a my_system_2.0b3 folder with
        disks/
           linux-bigswap2.img 
           linux-latest.img
        binaries/
            console
            ts_osfpal
            vmlinux

Step 3:
wget http://www.cs.utexas.edu/~parsec_m5/x86_64-vmlinux-2.6.28.4-smp

Step 4:
mv x86_64-vmlinux-2.6.28.4-smp m5_system_2.0b3/binaries/

Step 5:
wget http://www.cs.utexas.edu/~parsec_m5/x86root-parsec.img.bz2

Step 6:
bunzip2 x86root-parsec.img.bz2

Step 7:
mv x86root-parsec.img m5_system_2.0b3/disks/

Step 8:
Copy and apply patches/fullsystem_setup patch in your gem5 repo (hg qpush fullsystem_setup)
or
manually make the following changes:

diff -r 08baef92ce7a configs/common/Benchmarks.py
--- a/configs/common/Benchmarks.py  Sat Mar 05 20:18:41 2016 -0500
+++ b/configs/common/Benchmarks.py  Sun Mar 06 00:12:07 2016 -0500
@@ -57,7 +57,7 @@
         elif buildEnv['TARGET_ISA'] == 'alpha':
             return env.get('LINUX_IMAGE', disk('linux-latest.img'))
         elif buildEnv['TARGET_ISA'] == 'x86':
-            return env.get('LINUX_IMAGE', disk('x86root.img'))
+            return env.get('LINUX_IMAGE', disk('x86root-parsec.img'))
         elif buildEnv['TARGET_ISA'] == 'arm':
             return env.get('LINUX_IMAGE', disk('linux-aarch32-ael.img'))
         else:
diff -r 08baef92ce7a configs/common/FSConfig.py
--- a/configs/common/FSConfig.py    Sat Mar 05 20:18:41 2016 -0500
+++ b/configs/common/FSConfig.py    Sun Mar 06 00:12:07 2016 -0500
@@ -621,7 +621,8 @@
     if not cmdline:
         cmdline = 'earlyprintk=ttyS0 console=ttyS0 lpj=7999923 root=/dev/hda1'
     self.boot_osflags = fillInCmdline(mdesc, cmdline)
-    self.kernel = binary('x86_64-vmlinux-2.6.22.9')
+    #self.kernel = binary('x86_64-vmlinux-2.6.22.9')
+    self.kernel = binary('x86_64-vmlinux-2.6.28.4-smp')
     return self
 
 
diff -r 08baef92ce7a configs/common/SysPaths.py
--- a/configs/common/SysPaths.py    Sat Mar 05 20:18:41 2016 -0500
+++ b/configs/common/SysPaths.py    Sun Mar 06 00:12:07 2016 -0500
@@ -57,7 +57,7 @@
         try:
             path = env['M5_PATH'].split(':')
         except KeyError:
-            path = [ '/dist/m5/system', '/n/poolfs/z/dist/m5/system' ]
+            path = [ '/dist/m5/system', '/nethome/tkrishna3/gem5_fs_resources/m5_system_2.0b3/' ]
 
         # expand '~' and '~user' in paths
         path = map(os.path.expanduser, path)
diff -r 08baef92ce7a src/arch/x86/cpuid.cc
--- a/src/arch/x86/cpuid.cc Sat Mar 05 20:18:41 2016 -0500
+++ b/src/arch/x86/cpuid.cc Sun Mar 06 00:12:07 2016 -0500
@@ -154,7 +154,7 @@
                 break;
               case FamilyModelStepping:
                 result = CpuidResult(0x00020f51, 0x00000805,
-                                     0xe7dbfbff, 0x04000209);
+                                     0xe7dbfbff, 0x00000209);
                 break;
               default:
                 warn("x86 cpuid: unimplemented function %u", funcNum);

