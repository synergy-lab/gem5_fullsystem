# gem5_fullsystem

- gem5 with parsec2.1 checkpoints and patches to run them

- Developed by: Tushar Krishna (tushar@ece.gatech.edu)

- Last Updated: Feb 28, 2017

### gem5 version: from December 2015 with patches for garnet2.0 applied.

### ATTN: Size of Repo: 12G

## Useful files:

- gem5_fs_resources: scripts to create your own checkpoints

- m5_system_2.0b3: full-system binaries [ATTENTION: 4.2GB]

- checkpoints: parsec2.1 64-core checkpoints [ATTENTION: 3.9GB]

## Update environment:

- update configs/common/SysPaths.py to point to the location of m5_system_2.0b3

- setenv M5_PATH in your .cshrc file to point to the location of m5_system_2.0b3

## How to build and run:

- see example in gem5_fs_run_example.docx
